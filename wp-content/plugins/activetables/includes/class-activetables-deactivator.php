<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://somecompany.com
 * @since      1.0.0
 *
 * @package    Activetables
 * @subpackage Activetables/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Activetables
 * @subpackage Activetables/includes
 * @author     Vasily Ogar <ogar.vasily@gmail.com>
 */
class Activetables_Deactivator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function deactivate()
    {
        var_dump('deactivated');
    }

}
