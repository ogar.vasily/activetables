<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://somecompany.com
 * @since      1.0.0
 *
 * @package    Activetables
 * @subpackage Activetables/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Activetables
 * @subpackage Activetables/includes
 * @author     Vasily Ogar <ogar.vasily@gmail.com>
 */
class Activetables
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Activetables_Loader $loader Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->admin_ajax_actions();
        $this->define_public_hooks();

    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Activetables_Loader. Orchestrates the hooks of the plugin.
     * - Activetables_i18n. Defines internationalization functionality.
     * - Activetables_Admin. Defines all hooks for the admin area.
     * - Activetables_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies()
    {

        /**
         * Load ajax actions
         */
        require_once WPAT_PLUGIN_DIR . 'admin/ajax-actions.php';
        require_once WPAT_PLUGIN_DIR . 'public/ajax-actions.php';

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once WPAT_PLUGIN_DIR . 'includes/class-activetables-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once WPAT_PLUGIN_DIR . 'includes/class-activetables-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once WPAT_PLUGIN_DIR . 'admin/class-activetables-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once WPAT_PLUGIN_DIR . 'public/class-activetables-public.php';

        $this->loader = new Activetables_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Activetables_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale()
    {

        $plugin_i18n = new Activetables_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks()
    {

        $plugin_admin = new Activetables_Admin();

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
        $this->loader->add_action('admin_menu', $plugin_admin, 'admin_menu');
        $this->loader->add_action('in_admin_header', $plugin_admin, 'add_options');
        $this->loader->add_filter('set-screen-option', $plugin_admin, 'set_option', 10, 3);
        $this->loader->add_filter('upload_mimes', $plugin_admin, 'extend_mime_types', 1, 1);
    }

    /**
     * Register all ajax actions related to the admin area
     */
    private function admin_ajax_actions()
    {
        $ajax_actions = new Activetables_Admin_Ajax();
        add_action('wp_ajax_save_table', array($ajax_actions, 'save_table'));
        add_action('wp_ajax_save_settings', array($ajax_actions, 'save_settings'));
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks()
    {

        $plugin_public = new Activetables_Public();

        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');

    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Activetables_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader()
    {
        return $this->loader;
    }

}
