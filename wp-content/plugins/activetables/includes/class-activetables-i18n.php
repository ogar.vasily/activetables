<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://somecompany.com
 * @since      1.0.0
 *
 * @package    Activetables
 * @subpackage Activetables/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Activetables
 * @subpackage Activetables/includes
 * @author     Vasily Ogar <ogar.vasily@gmail.com>
 */
class Activetables_i18n
{


    /**
     * Load the plugin text domain for translation.
     *
     * @since    1.0.0
     */
    public function load_plugin_textdomain()
    {

        load_plugin_textdomain(
            WPAT_PLUGIN_NAME,
            false,
            WPAT_PLUGIN_DIR . '/languages/'
        );

    }


}
