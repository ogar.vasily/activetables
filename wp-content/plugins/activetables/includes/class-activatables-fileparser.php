<?php

class Activatables_FileParser
{
    public function generate_table()
    {
        $result = array();

        do_action('activetables_action_generate_table', $source, $content);

        $table_params = array('limit' => '10');
        switch ($source) {
            case 'csv' :
            case 'xls' :
                try {
                    $this->excelBasedConstruct($content, $table_params);
                    $result['table'] = $tbl;
                } catch (Exception $e) {
                    $result['error'] = $e->getMessage();
                    return $result;
                }
                break;
            case 'xml' :
                try {
                    $this->XMLBasedConstruct($content, $table_params);
                    $result['table'] = $tbl;
                } catch (Exception $e) {
                    $result['error'] = $e->getMessage();
                    return $result;
                }
                break;
            case 'json' :
                try {
                    $this->jsonBasedConstruct($content, $table_params);
                    $result['table'] = $tbl;
                } catch (Exception $e) {
                    $result['error'] = $e->getMessage();
                    return $result;
                }
                break;
            case 'serialized' :
                try {
                    $array = unserialize(WDTTools::curlGetData($content));
                    $this->arrayBasedConstruct($array, $table_params);
                    $result['table'] = $this;
                } catch (Exception $e) {
                    $result['error'] = $e->getMessage();
                    return $result;
                }
                break;
            case 'google_spreadsheet':
                try {
                    $array = WDTTools::extractGoogleSpreadsheetArray($content);
                    $this->arrayBasedConstruct($array, $table_params);
                    $result['table'] = $this;
                } catch (Exception $e) {
                    $result['error'] = $e->getMessage();
                    return $result;
                }
                break;
        }

        $result = apply_filters('activetables_filter_generate_table_result', $result);

        return $result;
    }

    public function source_excel($url, $wdtParameters = array())
    {
        ini_set("memory_limit", "2048M");

        if (!file_exists($url)) {
            throw new Error('File ' . stripcslashes($url) . ' does not exist!');
        }
        require_once(WPAT_PLUGIN_DIR . '/libs/phpexcel/PHPExcel.php');
        if (strpos(strtolower($url), '.xlsx')) {
            $PHPExcel_Reader = new PHPExcel_Reader_Excel2007();
            $PHPExcel_Reader->setReadDataOnly(true);
        } elseif (strpos(strtolower($url), '.xls')) {
            $PHPExcel_Reader = new PHPExcel_Reader_Excel5();
            $PHPExcel_Reader->setReadDataOnly(true);
        } elseif (strpos(strtolower($url), '.ods')) {
            $PHPExcel_Reader = new PHPExcel_Reader_OOCalc();
            $PHPExcel_Reader->setReadDataOnly(true);
        } elseif (strpos(strtolower($url), '.csv')) {
            $PHPExcel_Reader = new PHPExcel_Reader_CSV();
        } else {
            throw new Error('File format not supported!');
        }
        $PHPExcel = new PHPExcel();
        $PHPExcel = $PHPExcel_Reader->load($url);
        $objWorksheet = $PHPExcel->getActiveSheet();
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        $headingsArray = $objWorksheet->rangeToArray('A1:' . $highestColumn . '1', null, true, true, true);
        $headingsArray = $headingsArray[1];

        $r = -1;
        $namedDataArray = array();
        for ($row = 2; $row <= $highestRow; ++$row) {
            $dataRow = $objWorksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, true, true);
            if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                ++$r;
                foreach ($headingsArray as $dataColumnIndex => $dataColumnHeading) {
                    $namedDataArray[$r][$dataColumnHeading] = $dataRow[$row][$dataColumnIndex];
                    if (WDT_DETECT_DATES_IN_EXCEL) {
                        $cellID = $dataColumnIndex . $row;
                        if (PHPExcel_Shared_Date::isDateTime($PHPExcel->getActiveSheet()->getCell($cellID))) {
                            $namedDataArray[$r][$dataColumnHeading] = PHPExcel_Shared_Date::ExcelToPHP($dataRow[$row][$dataColumnIndex]);
                        }
                    }
                }
            }
        }

        $namedDataArray = apply_filters('wpdatatables_filter_excel_array', $namedDataArray, $this->getWpId(), $url);

        return $this->arrayBasedConstruct($namedDataArray, $wdtParameters);
    }
}