<?php

/**
 * Fired during plugin activation
 *
 * @link       http://somecompany.com
 * @since      1.0.0
 *
 * @package    Activetables
 * @subpackage Activetables/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Activetables
 * @subpackage Activetables/includes
 * @author     Vasily Ogar <ogar.vasily@gmail.com>
 */
class Activetables_Activator
{

    public static function update_db_check()
    {
        global $wpat_db_version;
        if (get_site_option('wpat_db_version') != $wpat_db_version) {
            Activetables_Activator::activate();
        }
    }

    /**
     * Setup default tables
     *
     * @since    1.0.0
     */
    public static function activate()
    {
        global $wpat_db_version;
        $wpat_db_version = '1.1';

        $create_sets = "CREATE TABLE " . WPAT_TBL_SETS . " (
						id INT(11) NOT NULL AUTO_INCREMENT,
						title VARCHAR(255) NOT NULL,
                        show_title TINYINT(1) NOT NULL default '1',
						source VARCHAR(10) NOT NULL,
						source_url TEXT NOT NULL,
						filtering TINYINT(1) NOT NULL default '1',
						sorting TINYINT(1) NOT NULL default '1',
						column_menus TINYINT(1) NOT NULL default '1',
						grid_menu_show_hide_columns TINYINT(1) NOT NULL default '1',
						show_grid_footer TINYINT(1) NOT NULL default '1',
						show_column_footer TINYINT(1) NOT NULL default '1',
						row_height INT(3) NOT NULL,
						min_column_size INT(3) NOT NULL,
						table_width VARCHAR(4) NOT NULL default '',
						table_height VARCHAR(4) NOT NULL default '',
						export TINYINT(1) NOT NULL default '0',
						pinning TINYINT(1) NOT NULL default '0',
						column_resizing TINYINT(1) NOT NULL default '0',
						current_lang VARCHAR (5) NOT NULL default '',
						word_wrap TINYINT(1) NOT NULL default '0',
						on_load TINYINT(1) NOT NULL default '0',
						UNIQUE KEY id (id)
						) DEFAULT CHARSET=utf8 COLLATE utf8_general_ci";


        $create_cols = "CREATE TABLE " . WPAT_TBL_COLS . " (
						id INT(11) NOT NULL AUTO_INCREMENT,
						table_id INT(11) NOT NULL,
						col_name VARCHAR(255) NOT NULL,
						filtering TINYINT(1) NOT NULL default '1',
                        cell_filter ENUM('text','number-range','date-range','select','checkbox') NOT NULL,
                        filter_placeholder VARCHAR(255) NOT NULL,
                        filter_options TEXT NOT NULL default '',
						sorting TINYINT(1) NOT NULL default '1',
						default_sort TINYINT(1) NOT NULL default '-1',
						column_type ENUM('auto','string','number','boolean','date') NOT NULL,
						hidden TINYINT(1) NOT NULL default '0',
						width VARCHAR(4) NOT NULL default '',
						header_cell_class VARCHAR(255) NOT NULL default '',
						cell_class VARCHAR(255) NOT NULL default '',
						footer_cell_class VARCHAR(255) NOT NULL default '',
						text_before VARCHAR(255) NOT NULL default '',
						text_after VARCHAR(255) NOT NULL default '',
						column_menu TINYINT(1) NOT NULL default '1',
						pinning TINYINT(1) NOT NULL default '1',
						pinned_side ENUM('left', 'right'),
						column_resizing TINYINT(1) NOT NULL default '0',
						order_position INT(3) NOT NULL default '0',
						aggregation_type ENUM('sum','avg','min','max'),
						UNIQUE KEY id (id)
						) DEFAULT CHARSET=utf8 COLLATE utf8_general_ci";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($create_sets);
        dbDelta($create_cols);

        add_option('wpat_db_version', $wpat_db_version);

        if (!get_option('wpat_default_language')) {
            update_option('wpat_default_language', '');
        }
        if (!get_option('wpat_date_format')) {
            update_option('wpat_date_format', 'd/m/Y');
        }
        if (!get_option('wpat_number_format')) {
            update_option('wpat_number_format', 1);
        }
        if (!get_option('wpat_fraction_size')) {
            update_option('wpat_fraction_size', 2);
        }
        if (!get_option('wpat_purchase_code')) {
            update_option('wpat_purchase_code', '');
        }
        if (!get_option('wpat_custom_js')) {
            update_option('wpat_custom_js', '');
        }
        if (!get_option('wpat_custom_css')) {
            update_option('wpat_custom_css', '');
        }
        if (!get_option('wpat_minified')) {
            update_option('wpat_minified', true);
        }

        // Tables update
        $installed_ver = get_option("wpat_db_version");
        if ($installed_ver != $wpat_db_version) {
            $sql = "CREATE TABLE " . WPAT_TBL_SETS . " (
						id int(11) NOT NULL AUTO_INCREMENT,
						title varchar(255) NOT NULL,
                        show_title tinyint(1) NOT NULL DEFAULT '1',
						source varchar(10) NOT NULL,
						source_url text NOT NULL,
						filtering tinyint(1) NOT NULL DEFAULT '0',
						sorting tinyint(1) NOT NULL DEFAULT '1',
						column_menus tinyint(1) NOT NULL DEFAULT '1',
						grid_menu_show_hide_columns tinyint(1) NOT NULL DEFAULT '1',
						grid_footer tinyint(1) NOT NULL DEFAULT '0',
						column_footer tinyint(1) NOT NULL DEFAULT '0',
						row_height int(3) NOT NULL,
						min_column_size int(3) NOT NULL,
						table_width varchar(4) NOT NULL DEFAULT '',
						table_height varchar(4) NOT NULL DEFAULT '',
						export tinyint(1) NOT NULL DEFAULT '0',
						pinning tinyint(1) NOT NULL DEFAULT '1',
						column_resizing tinyint(1) NOT NULL DEFAULT '0',
						table_lang varchar (5) NOT NULL DEFAULT '',
						word_wrap tinyint(1) NOT NULL DEFAULT '0',
						on_load tinyint(1) NOT NULL DEFAULT '0',
						UNIQUE KEY id (id)
						) DEFAULT CHARSET=utf8 COLLATE utf8_general_ci";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);

            update_option("wpat_db_version", $wpat_db_version);
        }
    }
}
