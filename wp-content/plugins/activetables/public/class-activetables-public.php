<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://somecompany.com
 * @since      1.0.0
 *
 * @package    Activetables
 * @subpackage Activetables/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Activetables
 * @subpackage Activetables/public
 * @author     Vasily Ogar <ogar.vasily@gmail.com>
 */
class Activetables_Public {

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Activetables_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Activetables_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( WPAT_PLUGIN_NAME, WPAT_PLUGIN_URL . 'css/activetables-public.css', array(), WPAT_PLUGIN_VERSION, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Activetables_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Activetables_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( WPAT_PLUGIN_NAME, WPAT_PLUGIN_URL . 'js/activetables-public.js', array( 'jquery' ), WPAT_PLUGIN_VERSION, false );

	}

}
