<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://somecompany.com
 * @since             1.0.0
 * @package           Activetables
 *
 * @wordpress-plugin
 * Plugin Name:       Active Tables
 * Plugin URI:        http://wpactivetables.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Vasily Ogar
 * Author URI:        http://somecompany.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       activetables
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

// Setting constants
global $wpdb;
define('WPAT_PLUGIN_NAME', 'activetables');
define('WPAT_PLUGIN_VERSION', '1.0.0');
define('WPAT_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('WPAT_PLUGIN_URL', plugin_dir_url(__FILE__));
define('WPAT_TBL_SETS', $wpdb->prefix . 'activetables_tables');
define('WPAT_TBL_COLS', $wpdb->prefix . 'activetables_columns');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-activetables-activator.php
 */
function activate_activetables()
{
    require_once WPAT_PLUGIN_DIR . 'includes/class-activetables-activator.php';
    Activetables_Activator::activate();
}

/**
 * The code runs when plugin loaded.
 * This action is documented in includes/class-activetables-activator.php
 */
function update_db_activetables()
{
    require_once WPAT_PLUGIN_DIR . 'includes/class-activetables-activator.php';
    Activetables_Activator::update_db_check();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-activetables-deactivator.php
 */
function deactivate_activetables()
{
    require_once WPAT_PLUGIN_DIR . 'includes/class-activetables-deactivator.php';
    Activetables_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_activetables');
register_deactivation_hook(__FILE__, 'deactivate_activetables');
add_action('plugins_loaded', 'update_db_activetables');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require WPAT_PLUGIN_DIR . 'includes/class-activetables.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_activetables()
{

    $plugin = new Activetables();
    $plugin->run();

}

run_activetables();
