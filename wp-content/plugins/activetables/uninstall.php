<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * @link       http://somecompany.com
 * @since      1.0.0
 *
 * @package    Activetables
 */

// If uninstall not called from WordPress, then exit.
if (!defined('WP_UNINSTALL_PLUGIN')) {
    exit;
}

delete_option('wpat_default_language');
delete_option('wpat_date_format');
delete_option('wpat_number_format');
delete_option('wpat_fraction_size');
delete_option('wpat_purchase_code');
delete_option('wpat_custom_js');
delete_option('wpat_custom_css');
delete_option('wpat_minified');

global $wpdb;
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}activetables_settings");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}activetables_columns");
