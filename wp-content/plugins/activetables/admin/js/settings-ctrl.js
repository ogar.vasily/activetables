app.controller('AdminSettings', ['$scope', '$http', function ($scope, $http) {
    $scope.form = {};

    // Form submit
    $scope.submitForm = function () {
        console.log($scope.form);
        $scope.focused = false;
        $scope.required = false;
        var config = {
            params: {
                action: 'save_settings'
            }
        }
        $http.post(ajaxurl, $scope.form, config).then(
            function (response) {
                $scope.message = response.data.message;
            }, function (errors) {
                $scope.message = errors.data.message;
            }
        );
    }
}]);