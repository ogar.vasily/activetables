app.controller('AdminEdit', ['$scope', '$http', function ($scope, $http) {
    $scope.sourceArr = ['csv', 'xml', 'xls', 'json'];
    $scope.form = {};
    $scope.form.table_id = '';
    $scope.form.table_title = '';
    $scope.form.show_title = 1;
    $scope.form.table_source = '';
    $scope.form.file_url = '';
    $scope.form.table_lang = '';
    $scope.form.table_width = '';
    $scope.form.table_height = '';
    $scope.form.row_height = '';
    $scope.form.column_size = '';
    $scope.form.column_menus = 1;
    $scope.form.on_load = 0;
    $scope.form.menu_show_hide = 1;
    $scope.form.filtering = 0;
    $scope.form.sorting = 1;
    $scope.form.pinning = 1;
    $scope.form.column_resizing = 0;
    $scope.form.export = 0;
    $scope.form.word_wrap = 0;
    $scope.form.grid_footer = 0;
    $scope.form.column_footer = 0;

    // File upload
    $scope.fileUpload = function () {
        function getExtension(filename) {
            var parts = filename.split('.');
            return parts[parts.length - 1];
        }

        function allowedFomats(filename) {
            var ext = getExtension(filename);
            switch (ext.toLowerCase()) {
                case 'csv':
                case 'xml':
                case 'xls':
                case 'xlsx':
                case 'json':
                    return true;
            }
            return false;
        }

        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }

        //Extend the wp.media object
        var custom_uploader = wp.media.frames.file_frame = wp.media({
            title: admin.media_upload_btn_title,
            button: {
                text: admin.media_upload_btn_title
            },
            multiple: false
        });

        //When a file is selected, grab the URL and set it as the text
        custom_uploader.on('select', function () {
            console.log(custom_uploader.state().get('selection').toJSON());
            var attachment = custom_uploader.state().get('selection').first().toJSON();
            console.log(attachment)
            if (!allowedFomats(attachment.filename)) {
                alert(admin.media_upload_alert);
                return;
            }

            $scope.$apply(function () {
                console.log($scope.file_url);
                $scope.file_url = attachment.url;
            });
        });

        //Open the uploader dialog
        custom_uploader.open();
    }

    // Form submit
    $scope.submitForm = function () {
        console.log($scope.form);
        $scope.focused = false;
        $scope.required = false;
        var config = {
            params: {
                action: 'save_table'
            }
        }
        $http.post(ajaxurl, $scope.form, config).then(function (data) {
            console.log(data);
        }, function (errors) {

        });
    }
}
]);