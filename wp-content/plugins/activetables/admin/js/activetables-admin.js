'use strict';
var app = angular.module('ActiveTables', []);

// Filters
app.filter('inArray', function () {
    return function (array, value) {
        return array.indexOf(value) !== -1;
    };
});

angular.element(document).ready(function () {
    angular.bootstrap(document, ['ActiveTables']);
});