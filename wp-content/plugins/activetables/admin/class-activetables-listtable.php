<?php
/**
 * Output table with list of existing activetables
 */

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class WPAT_List_Table extends WP_List_Table
{

    function prepare_items()
    {
        global $wpdb;

        /* -- Preparing query -- */
        $query = "SELECT id, title, source FROM " . WPAT_TBL_SETS . " ";

        /* -- Ordering parameters -- */
        //Parameters that are going to be used to order the result
        $orderby = !empty($_GET["orderby"]) ? esc_sql($_GET["orderby"]) : 'ASC';
        $order = !empty($_GET["order"]) ? esc_sql($_GET["order"]) : 'id';
        $query .= ' ORDER BY ' . $order . ' ' . $orderby;

        /* -- Pagination parameters -- */
        //Number of elements in your table?
        $total_items = $wpdb->query($query); //return the total number of affected rows

        //How many to display per page?
        $per_page = $this->get_items_per_page('activetables_per_page', 10);

        //Which page is this?
        $paged = !empty($_GET["paged"]) ? esc_sql($_GET["paged"]) : '';

        //Page Number
        if (empty($paged) || !is_numeric($paged) || $paged <= 0) {
            $paged = 1;
        }

        //How many pages do we have in total?
        $total_pages = ceil($total_items / $per_page);

        //adjust the query to take pagination into account
        if (!empty($paged) && !empty($per_page)) {
            $offset = ($paged - 1) * $per_page;
            $query .= ' LIMIT ' . (int)$offset . ',' . (int)$per_page;
        }

        /* -- Register the pagination -- */
        $this->set_pagination_args(array(
            "total_items" => $total_items,
            "total_pages" => $total_pages,
            "per_page" => $per_page,
        ));
        //The pagination links are automatically built according to those parameters

        /* -- Register the Columns -- */
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);

        /* -- Fetch the items -- */
        $this->items = $wpdb->get_results($query);
    }

    function get_columns()
    {
        $cols = array(
            'cb' => '<input type="checkbox" />',
            'id' => 'ID',
            'title' => 'Title',
            'source' => 'Source',
            'shortcode' => 'Shortcode'
        );
        return $cols;
    }

    function get_sortable_columns()
    {
        return array(
            'id' => array('id', true),
            'title' => array('title', false),
            'source' => array('source', false)
        );
    }

    function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'shortcode':
                return '[ACTIVETABLE ID=' . $item->id . ']';
                break;
            default:
                return $item->$column_name;
                break;
        }
    }

    function column_title($item)
    {
        $actions = array(
            'edit' => '<a href="admin.php?page=activetables&action=edit&table_id=' . $item->id . '" title="' . __('Edit', 'activetables') . '">' . __('Edit', 'activetables') . '</a>',
            'trash' => '<a class="submitdelete" title="' . __('Delete', 'activetables') . '" href="admin.php?page=activetables&action=delete&table_id=' . $item->id . '" rel="' . $item->id . '">' . __('Delete', 'activetables') . '</a>'
        );

        return '<a href="admin.php?page=activetables&action=edit&table_id=' . $item->id . '">' . $item->title . '</a> ' . $this->row_actions($actions);

    }

    function get_bulk_actions()
    {
        $actions = array(
            'delete' => __('Delete', 'activetables')
        );
        return $actions;
    }

    function column_cb($item)
    {
        return '<input type="checkbox" name="table_id[]" value="' . $item->id . '" />';
    }

    function no_items()
    {
        _e("Still no tables", 'activetables');
    }

}