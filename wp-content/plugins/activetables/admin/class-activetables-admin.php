<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://somecompany.com
 * @since      1.0.0
 *
 * @package    Activetables
 * @subpackage Activetables/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Activetables
 * @subpackage Activetables/admin
 * @author     Vasily Ogar <ogar.vasily@gmail.com>
 */
class Activetables_Admin
{
    private $translation_array;

    function __construct()
    {
        $this->translation_array = array(
            'media_upload_page_title' => __('Choose file', 'activetables'),
            'media_upload_btn_title' => __('Choose file', 'activetables'),
            'media_upload_alert' => __('Please select a valid file format CSV, XML, XLS, XLSX or JSON', 'activetables'),
        );
    }

    public static function get_table_data($id)
    {
        global $wpdb;

        do_action('activetables_action_before_get_table_data', $id);

        $query = $wpdb->prepare("SELECT * FROM " . WPAT_TBL_SETS . " WHERE id=%d", $id);
        $data = $wpdb->get_row($query);

        $data = apply_filters('activetables_filter_table_data', $data, $id);

        return $data;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {
        wp_enqueue_style(WPAT_PLUGIN_NAME . '-bootstrap', WPAT_PLUGIN_URL . 'libs/bootstrap/bootstrap.min.css', array(), WPAT_PLUGIN_VERSION, 'all');
        wp_enqueue_style(WPAT_PLUGIN_NAME . '-font-awesome', WPAT_PLUGIN_URL . 'admin/css/font-awesome.min.css', array(), WPAT_PLUGIN_VERSION, 'all');
        wp_enqueue_style(WPAT_PLUGIN_NAME, WPAT_PLUGIN_URL . 'admin/css/activetables-admin.css', array(), WPAT_PLUGIN_VERSION, 'all');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {
        wp_enqueue_script(WPAT_PLUGIN_NAME . '-angular', WPAT_PLUGIN_URL . 'libs/angular/angular.min.js', array('jquery'), WPAT_PLUGIN_VERSION, true);
//        wp_enqueue_script(WPAT_PLUGIN_NAME . '-bootstrap', WPAT_PLUGIN_URL . 'libs/bootstrap/bootstrap.min.js', array('jquery'), WPAT_PLUGIN_VERSION, true);
        wp_enqueue_script(WPAT_PLUGIN_NAME . '-admin', WPAT_PLUGIN_URL . 'admin/js/activetables-admin.js', array('activetables-angular'), WPAT_PLUGIN_VERSION, true);
        wp_localize_script(WPAT_PLUGIN_NAME . '-admin', 'admin', $this->translation_array);
    }

    /**
     * Generate menu items
     */
    public function admin_menu()
    {
        add_menu_page('ActiveTables', 'ActiveTables', 'manage_options', 'activetables', array($this, 'all_tables_page'), 'none');
        add_submenu_page('activetables', 'Add a new table', 'Add new table', 'manage_options', 'activetables-add-table', array($this, 'add_table_page'));
        add_submenu_page('activetables', 'ActiveTables settings', 'Settings', 'manage_options', 'activetables-settings', array($this, 'settings_page'));
    }

    /**
     * Output all existing tables
     */
    function all_tables_page()
    {
        global $wpdb;
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'activetables'));
        }
        $action = isset($_GET['action']) ? filter_var($_GET['action'], FILTER_SANITIZE_STRING) : '';
        if ($action == 'edit') {
            ob_start();
            include_once(WPAT_PLUGIN_DIR . 'admin/partials/edit-table.tpl.php');
            $page_content = ob_get_contents();
            ob_end_clean();

            $edit_table = apply_filters('activetables_filter_edit_table', $page_content);
            echo $edit_table;
        } else {
            if ($action == 'delete') {
                $table_id = $_GET['table_id'];

                if (!is_array($table_id)) {
                    $wpdb->delete(WPAT_TBL_SETS, array('ID' => $table_id), array('%d'));
                    $wpdb->delete(WPAT_TBL_COLS, array('table_id' => $table_id), array('%d'));
                } else {
                    foreach ($table_id as $id) {
                        $wpdb->delete(WPAT_TBL_SETS, array('ID' => $id), array('%d'));
                        $wpdb->delete(WPAT_TBL_COLS, array('table_id' => $id), array('%d'));
                    }
                }
            }

            ob_start();
            include_once(WPAT_PLUGIN_DIR . 'admin/partials/all-tables.tpl.php');
            $page_content = ob_get_contents();
            ob_end_clean();

            $all_tables = apply_filters('activetables_filter_all_tables', $page_content);
            echo $all_tables;
        }

        do_action('activetables_action_all_tables');
    }

    /**
     * Add new table
     */
    public function add_table_page()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'activetables'));
        }

        ob_start();
        include_once(WPAT_PLUGIN_DIR . 'admin/partials/edit-table.tpl.php');
        $page_content = ob_get_contents();
        ob_end_clean();

        $add_table = apply_filters('activetables_filter_add_table', $page_content);
        echo $add_table;

        do_action('activetables_action_add_table');
    }

    /**
     * Settings page
     */
    public function settings_page()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'activetables'));
        }

        ob_start();
        include_once(WPAT_PLUGIN_DIR . 'admin/partials/settings.tpl.php');
        $page_content = ob_get_contents();
        ob_end_clean();

        $settings_page = apply_filters('activetables_filter_settings_page', $page_content);

        echo $settings_page;

        do_action('activetables_action_settings_page');
    }

    /**
     * Add option to Screen options menu
     */
    public function add_options()
    {
        $option = 'per_page';
        $args = array(
            'label' => 'Tables per page',
            'default' => 10,
            'option' => 'activetables_per_page'
        );
        add_screen_option($option, $args);
    }

    /**
     * "Screen Options" slide-in where the user can adjust the columns to be shown and the number of rows to be displayed
     */
    public function set_option($status, $option, $value)
    {
        return $value;
    }

    /**
     * Extend wordpress mime types
     */
    public function extend_mime_types($mime_types)
    {
        $mime_types['xml'] = 'application/xml';
        $mime_types['json'] = 'application/json';
        return $mime_types;
    }
}
