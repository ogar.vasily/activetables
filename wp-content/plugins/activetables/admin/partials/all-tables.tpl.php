<div class="wrap">
    <h1>ActiveTables <a href="admin.php?page=activetables-add-table"
                        class="add-new-h2"><?php _e('Add new', 'wpdatatables'); ?></a></h1>

    <form method="post" action="<?php echo admin_url('admin.php?page=activetables'); ?>"
          id="wpat_all_tables">
        <?php
        require_once(WPAT_PLUGIN_DIR . 'admin/class-activetables-listtable.php');
        $list_table = new WPAT_List_Table();
        $list_table->prepare_items();
        echo $list_table->display();
        ?>
    </form>
</div>

<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {
            $('a.submitdelete').click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                if (confirm("<?php _e('Are you sure', 'activetables'); ?>?")) {
                    window.location = $(this).attr('href');
                }
            })

            $('#doaction').click(function (e) {
                e.preventDefault();

                if ($('#bulk-action-selector-top').val() == '') {
                    return;
                }
                if ($('#wpat_all_tables table.widefat input[type="checkbox"]:checked').length == 0) {
                    return;
                }

                if (confirm("<?php _e('Are you sure', 'activetables'); ?>?")) {
                    $('#wpat_all_tables').submit();
                }
            });
        });
    })(jQuery)
</script>
