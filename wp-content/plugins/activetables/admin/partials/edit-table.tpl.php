<?php
// Enqueue file with angular controller
wp_enqueue_script(WPAT_PLUGIN_NAME . '-edit-ctrl', WPAT_PLUGIN_URL . 'admin/js/edit-table-ctrl.js', array('activetables-admin'), WPAT_PLUGIN_VERSION, true);
// Add media uploader scripts
wp_enqueue_media();


//var_dump(get_allowed_mime_types());
$table_id = (isset($_GET['table_id'])) ? (int)$_GET['table_id'] : '';
if ($table_id) {
    $data = Activetables_Admin::get_table_data($table_id);
}
?>
<div class="wrap" ng-controller="AdminEdit">
    <h1>
        <?php if (!empty($table_id)) { ?>
            <?php _e('Edit table', 'activetables') ?>
            <a href="admin.php?page=activetables&action=delete&table_id=<?php echo $table_id ?>"
               class="btn btn-default wpat_delete"><?php _e('Delete', 'activetables'); ?></a>
        <?php } else { ?>
            <?php _e('Add new table', 'activetables') ?>
        <?php } ?>
    </h1>

    <?php if ($table_id) { ?>
        <div class="notice update-nag">
            <p><strong><?php _e('Please use shortcode to insert the table on your page', 'activetables'); ?>
                    [ACTIVETABLE ID=<?php if ($table_id) echo $table_id; ?>]</strong></p>
        </div>
    <?php } ?>

    <form method="post" id="wpat_save_table" ng-submit="submitForm()">
        <input type="hidden" id="wpat_table_id" ng-model="form.table_id"
               ng-init="form.table_id='<?php if (!empty($table_id)) echo $table_id ?>'"/>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?php _e('General settings', 'activetables'); ?></h3>
            </div>
            <div class="box-body">
                <div class="form-group clearfix">
                    <label for="wpat_table_title" class="col-sm-2">
                        <?php _e('Table title', 'activetables'); ?>
                    </label>
                    <div class="col-sm-5">
                        <span ng-show="!focused || error_table_title" class="error"></span>
                        <input type="text" id="wpat_table_title" name="wpat_table_title" class="form-control"
                               ng-init="form.table_title='<?php if (!empty($data->title)) echo esc_html($data->title) ?>'"
                               value="<?php if (!empty($data->title)) echo esc_html($data->title) ?>"
                               ng-model="form.table_title" ng-required="true" ng-focus="focused=true"/>
                        <div class="wpat_description">
                            <?php _e('It will appear above your table', 'activetables'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_show_table_title" class="col-sm-2">
                        <?php _e('Show table title', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_show_table_title" name="wpat_show_table_title"
                            <?php if ((isset($data->show_title) && ($data->show_title == 1)) || !isset($data->show_title)) { ?>
                                ng-init="form.show_title = true"
                            <?php } ?>
                               ng-false-value="0"
                               ng-model="form.show_title"/>
                        <span class="wpat_description">
                            <?php _e('If you want to hide table title on the page, untick the box', 'activetables'); ?>
                        </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_table_source" class="col-sm-2">
                        <?php _e('Source of the table content', 'activetables'); ?>
                    </label>
                    <div class="col-sm-2">
                        <select name="wpat_table_source" id="wpat_table_source" class="form-control"
                                ng-init="form.table_source='<?php if (!empty($data->source)) echo $data->source ?>'"
                                ng-model="form.table_source">
                            <option
                                value=""><?php _e('Select a table source...', 'activetables'); ?></option>
                            <optgroup label="<?php _e('General', 'activetables') ?>">
                                <option value="csv"
                                        <?php if (!empty($table_data['source']) && $table_data['source'] == 'csv') { ?>selected="selected"<?php } ?>><?php _e('CSV file', 'activetables'); ?></option>
                                <option value="xls"
                                        <?php if (!empty($table_data['source']) && $table_data['source'] == 'xls') { ?>selected="selected"<?php } ?>><?php _e('Excel file', 'activetables'); ?></option>
                                <option value="google_ss"
                                        <?php if (!empty($table_data['source']) && $table_data['source'] == 'google_ss') { ?>selected="selected"<?php } ?>><?php _e('Google spreadsheet', 'activetables'); ?></option>
                                <option value="xml"
                                        <?php if (!empty($table_data['source']) && $table_data['source'] == 'xml') { ?>selected="selected"<?php } ?>><?php _e('XML file', 'activetables'); ?></option>
                                <option value="json"
                                        <?php if (!empty($table_data['source']) && $table_data['source'] == 'json') { ?>selected="selected"<?php } ?>><?php _e('JSON file', 'activetables'); ?></option>
                            </optgroup>
                            <optgroup label="<?php _e('Advanced', 'activetables') ?>">
                                <option value="wp_query"
                                        <?php if (!empty($table_data['source']) && $table_data['source'] == 'wp_query') { ?>selected="selected"<?php } ?>><?php _e('WP_Query', 'activetables'); ?></option>
                                <option value="serialized"
                                        <?php if (!empty($table_data['source']) && $table_data['source'] == 'serialized') { ?>selected="selected"<?php } ?>><?php _e('Serialized PHP array', 'activetables'); ?></option>
                            </optgroup>
                        </select>
                    </div>
                    <div class="col-sm-8 wpat_description">
                        <?php _e('Choose a type of input source for your table', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group clearfix ng-hide" ng-show="sourceArr | inArray : form.table_source">
                    <label for="wpat_file_upload" class="col-sm-2">
                        <?php _e('Input file', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <div class="upload-response" ng-bind="file_url"
                             ng-init="form.file_url='<?php if (!empty($data->source) && in_array($data->source, array('csv, xls, xml, json'))) echo esc_url($data->source_url) ?>'"></div>
                        <a id="wpat_file_upload_btn" class="button" ng-click="fileUpload()">
                            <?php _e('Upload file', 'activetables') ?>
                        </a>
                        <span class="wpat_description">
                            <?php _e('Allowed file formats CSV, XML, JSON, XLS or XLSX', 'activetables'); ?>
                        </span>
                    </div>
                </div>


                <div class="form-group clearfix ng-hide" ng-show="form.table_source === 'google_ss'">
                    <label for="wpat_google_ss" class="col-sm-2">
                        <?php _e('Google Spreadsheet URL', 'activetables'); ?>
                    </label>
                    <div class="col-sm-5">
                        <input type="text" id="wpat_google_ss" name="wpat_google_ss" class="form-control"
                               ng-init="form.file_url='<?php if (!empty($data->source) && $data->source == 'google_ss') echo esc_url($data->source_url) ?>'"
                               value="<?php if (!empty($data->source) && $data->source == 'google_ss') echo esc_url($data->source_url) ?>"
                               ng-model="form.file_url"/>
                        <div class="wpat_description">
                            <?php _e('Before you insert a link, please publish the spreadsheet', 'activetables'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_table_lang" class="col-sm-2">
                        <?php _e('Table language', 'activetables'); ?>
                    </label>
                    <div class="col-sm-2">
                        <select name="wpat_table_lang" id="wpat_table_lang" class="form-control"
                                ng-init="form.table_lang='<?php if (isset($data->table_lang)) echo $data->table_lang; ?>'"
                                ng-model="form.table_lang">
                            <option
                                value=""><?php _e('Select a table language...', 'activetables'); ?></option>
                            <option value="mysql"
                                    <?php if (!empty($table_data['current_lang']) && $table_data['current_lang'] == 'mysql') { ?>selected="selected"<?php } ?>><?php _e('MySQL query', 'activetables'); ?></option>
                            <option value="csv"
                                    <?php if (!empty($table_data['current_lang']) && $table_data['current_lang'] == 'csv') { ?>selected="selected"<?php } ?>><?php _e('CSV file', 'activetables'); ?></option>
                            <option value="xls"
                                    <?php if (!empty($table_data['current_lang']) && $table_data['current_lang'] == 'xls') { ?>selected="selected"<?php } ?>><?php _e('Excel file', 'activetables'); ?></option>
                            <option value="google_spreadsheet"
                                    <?php if (!empty($table_data['current_lang']) && $table_data['current_lang'] == 'google_spreadsheet') { ?>selected="selected"<?php } ?>><?php _e('Google spreadsheet', 'activetables'); ?></option>
                            <option value="xml"
                                    <?php if (!empty($table_data['current_lang']) && $table_data['current_lang'] == 'xml') { ?>selected="selected"<?php } ?>><?php _e('XML file', 'activetables'); ?></option>
                            <option value="json"
                                    <?php if (!empty($table_data['current_lang']) && $table_data['current_lang'] == 'json') { ?>selected="selected"<?php } ?>><?php _e('JSON file', 'activetables'); ?></option>
                            <option value="serialized"
                                    <?php if (!empty($table_data['current_lang']) && $table_data['current_lang'] == 'serialized') { ?>selected="selected"<?php } ?>><?php _e('Serialized PHP array', 'activetables'); ?></option>
                        </select>
                    </div>
                    <div class="col-sm-8 wpat_description">
                        <?php _e('Choose a type of input source for your table', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_table_width" class="col-sm-2">
                        <?php _e('Table width', 'activetables'); ?>
                    </label>
                    <div class="col-sm-1">
                        <input type="text" id="wpat_table_width" name="wpat_table_width" class="form-control"
                               ng-init="form.table_width='<?php if (!empty($data->table_width)) echo $data->table_width; ?>'"
                               ng-model="form.table_width"/>
                    </div>
                    <div class="col-sm-9 wpat_description">
                        <?php _e('For Google Spreadsheets: please do not forget to publish the spreadsheet before pasting the URL.', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_table_height" class="col-sm-2">
                        <?php _e('Table height', 'activetables'); ?>
                    </label>
                    <div class="col-sm-1">
                        <input type="text" id="wpat_table_height" name="wpat_table_height" class="form-control"
                               ng-init="form.table_height='<?php if (!empty($data->table_height)) echo $data->table_height; ?>'"
                               ng-model="form.table_height"/>
                    </div>
                    <div class="col-sm-9 wpat_description">
                        <?php _e('For Google Spreadsheets: please do not forget to publish the spreadsheet before pasting the URL.', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">
                        <input type="submit" name="submitStep1"
                               class="submitStep1 button-primary"
                               value="<?php _e('Save', 'activetables'); ?>">
                        <button class="button-primary previewButton"
                                style="display: none"><?php _e('Preview', 'activetables'); ?></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border clearfix">
                <a ng-class="{collapsed: !set_hide}" href="#additional_settings"
                   ng-click="set_hide = set_hide ? false : true">
                    <h3 class="box-title"><?php _e('Additional settings', 'activetables'); ?></h3>
                    <i class="fa fa-plus"></i>
                    <i class="fa fa-minus"></i>
                </a>
                <div class="box-tools pull-right">
                    <a ng-class="{collapsed: !set_hide}" href="#additional_settings">
                        <i class="fa fa-plus"></i>
                        <i class="fa fa-minus"></i>
                    </a>
                </div>
            </div>
            <div class="box-body" id="additional_settings ng-hide" ng-hide="!set_hide">
                <div class="form-group clearfix">
                    <label for="wpat_row_height" class="col-sm-2">
                        <?php _e('Default row height', 'activetables'); ?>
                    </label>
                    <div class="col-sm-1">
                        <input type="text" id="wpat_row_height" name="wpat_row_height" class="form-control"
                               ng-init="form.row_height='<?php if (!empty($data->row_height)) echo $data->row_height; ?>'"
                               ng-model="form.row_height"/>
                    </div>
                    <div class="col-sm-9 wpat_description">
                        <?php _e('The height of the row in pixels, defaults to 30', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_column_size" class="col-sm-2">
                        <?php _e('Minimum column size', 'activetables'); ?>
                    </label>
                    <div class="col-sm-1">
                        <input type="text" id="wpat_column_size" name="wpat_column_size" class="form-control"
                               ng-init="form.column_size='<?php if (!empty($data->min_column_size)) echo $data->min_column_size; ?>'"
                               ng-model="form.column_size"/>
                    </div>
                    <div class="col-sm-9 wpat_description">
                        <?php _e('Columns can\'t be smaller than this, defaults to 10 pixels', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_column_menus" class="col-sm-2">
                        <?php _e('Column menus', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_column_menus"
                            <?php if (isset($data->column_menus) && $data->column_menus == 1 || !isset($data->column_menus)) { ?>
                                ng-init="form.column_menus = true"
                            <?php } ?>
                               ng-model="form.column_menus"/>
                                <span class="wpat_description">
                                    <?php _e('True by default. When enabled, this setting displays a column menu within each column.', 'activetables'); ?>
                                </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_menu_show_hide" class="col-sm-2">
                        <?php _e('Menu option "show/hide columns"', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_menu_show_hide"
                            <?php if (isset($data->grid_menu_show_hide_columns) && $data->grid_menu_show_hide_columns == 1 || !isset($data->grid_menu_show_hide_columns)) { ?>
                                ng-init="form.menu_show_hide=true"
                            <?php } ?>
                               ng-model="form.menu_show_hide"
                        />
                                <span class="wpat_description">
                                    <?php _e('True by default, whether the grid menu should allow hide/show of columns', 'activetables'); ?>
                                </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_show_onload" class="col-sm-2">
                        <?php _e('Show table on window load', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_show_onload"
                            <?php if (isset($data->on_load) && $data->on_load == 1) { ?>
                                ng-init="form.on_load=true"
                            <?php } ?>
                               ng-model="form.on_load"/>
                                <span class="wpat_description">
                                    <?php _e('False by default. Check this checkbox if you would prevent table from showing until the page loads completely. May be useful for slowly loading pages', 'activetables'); ?>
                                </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_filtering" class="col-sm-2">
                        <?php _e('Filtering', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_filtering"
                            <?php if (isset($data->filtering) && $data->filtering == 1) { ?>
                                ng-init="form.filtering=true"
                            <?php } ?>
                               ng-model="form.filtering"/>
                                <span class="wpat_description">
                                    <?php _e('False by default. When enabled, this setting adds filter boxes to each column header, allowing filtering within the column for the entire grid. Filtering can then be disabled on individual columns using the columnDefs', 'activetables'); ?>
                                </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_sorting" class="col-sm-2">
                        <?php _e('Sorting', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_sorting"
                            <?php if (isset($data->sorting) && $data->sorting == 1 || !isset($data->sorting)) { ?>
                                ng-init="form.sorting=true"
                            <?php } ?>
                               ng-model="form.sorting"/>
                                <span class="wpat_description">
                                    <?php _e('True by default. When enabled, this setting adds sort widgets to the column headers, allowing sorting of the data for the entire grid. Sorting can then be disabled on individual columns using the columnDefs.', 'activetables'); ?>
                                </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_pinning" class="col-sm-2">
                        <?php _e('Pinning', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_pinning"
                            <?php if (isset($data->pinning) && $data->pinning == 1 || !isset($data->pinning)) { ?>
                                ng-init="form.pinning=true"
                            <?php } ?>
                               ng-model="form.pinning"/>
                                <span class="wpat_description">
                                    <?php _e('True by default. Enable pinning for the individual column.', 'activetables'); ?>
                                </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_column_resizing" class="col-sm-2">
                        <?php _e('Column resizing', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_column_resizing"
                            <?php if (isset($data->column_resizing) && $data->column_resizing == 1) { ?>
                                ng-init="form.column_resizing=true"
                            <?php } ?>
                               ng-model="form.column_resizing"/>
                                <span class="wpat_description">
                                    <?php _e('False by default. Enable column resizing on an individual column', 'activetables'); ?>
                                </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_export" class="col-sm-2">
                        <?php _e('Export', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_export"
                            <?php if (isset($data->export) && $data->export == 1) { ?>
                                ng-init="form.export=true"
                            <?php } ?>
                               ng-model="form.export"/>
                                <span class="wpat_description">
                                    <?php _e('False by default. Ability to export data from the grid. Data can be exported in a range of formats, and all data, visible data, or selected rows can be exported, with all columns or visible columns.', 'activetables'); ?>
                                </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_word_wrap" class="col-sm-2">
                        <?php _e('Word wrap', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_word_wrap"
                            <?php if (isset($data->word_wrap) && $data->word_wrap == 1) { ?>
                                ng-init="form.word_wrap=true"
                            <?php } ?>
                               ng-false-value="0"
                               ng-model="form.word_wrap"/>
                                <span class="wpat_description">
                                    <?php _e('False by default. CSS rule to wrap words in the columns', 'activetables'); ?>
                                </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_grid_footer" class="col-sm-2">
                        <?php _e('Show grid footer', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_grid_footer"
                            <?php if (isset($data->grid_footer) && $data->grid_footer == 1) { ?>
                                ng-init="form.grid_footer=true"
                            <?php } ?>
                               ng-false-value="0"
                               ng-model="form.grid_footer"/>
                                <span class="wpat_description">
                                    <?php _e('False by default. The footer display Total Rows and Visible Rows (filtered rows)', 'activetables'); ?>
                                </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_column_footer" class="col-sm-2">
                        <?php _e('Show column footer', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_column_footer"
                            <?php if (isset($data->column_footer) && $data->column_footer == 1) { ?>
                                ng-init="form.column_footer=true"
                            <?php } ?>
                               ng-model="form.column_footer"/>
                                <span class="wpat_description">
                                    <?php _e('False by default. The column footer displays column aggregates', 'activetables'); ?>
                                </span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">
                        <button type="submit" class="submitStep1 btn btn-primary">
                            <?php _e('Save', 'activetables'); ?>
                        </button>
                        <button class="button-primary previewButton" style="display: none">
                            <?php _e('Preview', 'activetables'); ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>


        <div id="step2-postbox" class="meta-box-sortables ui-sortable" style="display: none">
            <div class="postbox">

                <div class="handlediv" title="<?php _e('Click to toggle', 'activetables'); ?>"><br/>
                </div>
                <h3 class="hndle" style="height: 27px">
                                    <span><div
                                            class="dashicons dashicons-exerpt-view"></div> <?php _e('Optional column setup', 'activetables'); ?></span>
                    <div class="pull-right" style="margin-right: 5px">
                        <input type="submit" name="submitStep2" class="button-primary submitStep2"
                               value="<?php _e('Save', 'activetables'); ?>">
                        <button
                            class="button-primary ungroupButton"><?php _e('Ungroup', 'activetables'); ?></button>
                        <button class="button-primary previewButton"
                                style="display: none"><?php _e('Preview', 'activetables'); ?></button>
                        <button
                            class="button-primary closeButton"><?php _e('Close', 'activetables'); ?></button>
                    </div>
                </h3>

                <div class="inside" style="overflow: scroll">
                    <table>
                        <tbody>
                        <tr class="step2_row">
                            <td colspan="2">
                                <p>
                                    <?php _e('You can change the column settings in this step, but this is not required, since default options have already been generated for you', 'activetables'); ?>
                                </p>
                                <span class="wpat_description">
                                    <strong><?php _e('Warning', 'activetables'); ?>:</strong>
                                    <?php _e('If you change the table settings, save the table before modifying the column settings, because the column set can be changed and you may lose your changes', 'activetables'); ?>
                                </span>
                            </td>
                        </tr>
                        <tr class="step2_row">
                            <td colspan="2" class="columnsBlock">
                            </td>
                        </tr>
                        <tr class="step2_row">
                            <td colspan="2">
                                <input type="submit" name="submitStep2"
                                       class="button-primary submitStep2"
                                       value="<?php _e('Save', 'activetables'); ?>">
                                <button
                                    class="button-primary ungroupButton"><?php _e('Ungroup', 'activetables'); ?></button>
                                <button class="button-primary previewButton"
                                        style="display: none"><?php _e('Preview', 'activetables'); ?></button>
                                <button
                                    class="button-primary closeButton"><?php _e('Close', 'activetables'); ?></button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

    var columns_data = <?php if (!empty($column_data)) {
        echo json_encode(stripslashes_deep($column_data));
    } else {
        echo json_encode(array());
    } ?>;
    var preview_called = false;

</script>