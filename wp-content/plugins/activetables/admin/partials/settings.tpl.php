<?php
// Enqueue file with angular controller
wp_enqueue_script(WPAT_PLUGIN_NAME . '-settings-ctrl', WPAT_PLUGIN_URL . 'admin/js/settings-ctrl.js', array('activetables-admin'), WPAT_PLUGIN_VERSION, true);

$default_language = get_option('wpat_default_language');
$languages = [];
$date_format = get_option('wpat_date_format');
$number_format = get_option('wpat_number_format');
$fraction_size = get_option('wpat_fraction_size');
$purchase_code = get_option('wpat_purchase_code');
$custom_css = get_option('wpat_custom_css');
$custom_js = get_option('wpat_custom_js');
$minified = get_option('wpat_minified');
?>
<div class="wrap" ng-controller="AdminSettings">
    <h1><?php _e('ActiveTables settings', 'activetables'); ?></h1>

    <div class="updated ng-hide" ng-show="message">
        <p><strong>{{message}}</strong></p>
    </div>

    <form method=" post" id="wpat_settings" ng-submit="submitForm()">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?php _e('Main settings', 'activetables'); ?></h3>
            </div>
            <div class="box-body">
                <div class="form-group clearfix">
                    <label for="wpat_default_lang" class="col-sm-2">
                        <?php _e('Default tables language', 'activetables'); ?>
                    </label>
                    <div class="col-sm-2">
                        <select name="wpat_default_lang" id="wpat_default_lang" class="form-control"
                                ng-init="form.default_lang='<?php if ($default_language) echo $default_language ?>'"
                                ng-model="form.default_lang">
                            <option value=""
                                    <?php if (empty($default_language)) { ?>selected="selected"<?php } ?> >
                                English (default)
                            </option>
                            <?php foreach ($languages as $language) { ?>
                                <option value="<?php echo $language['code'] ?>"
                                        <?php if ($default_language == $language['code']) { ?>selected="selected"<?php } ?> >
                                    <?php echo $language['title']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-8" class="wpat_description">
                        <?php _e('Please select the language which will be utilized as a part of tables interface', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label class="col-sm-2" for="wpat_date_format">
                        <?php _e('Date formatting', 'activetables'); ?>
                    </label>
                    <div class="col-sm-2">
                        <select name="wpat_date_format" id="wpat_date_format" class="form-control"
                                ng-init="form.date_format='<?php echo ($date_format) ? $date_format : 'd/m/y' ?>'"
                                ng-model="form.date_format">
                            <option value="d/m/y"
                                    <?php if ($date_format == 'd/m/y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('d/m/y') ?>
                            </option>
                            <option value="m/d/y"
                                    <?php if ($date_format == 'm/d/y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('m/d/y') ?>
                            </option>
                            <option value="y/m/d"
                                    <?php if ($date_format == 'y/m/d') { ?>selected="selected"<?php } ?> >
                                <?php echo date('y/m/d') ?>
                            </option>
                            <option value="d/m/Y"
                                    <?php if ($date_format == 'd/m/Y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('d/m/Y') ?>
                            </option>
                            <option value="m/d/Y"
                                    <?php if ($date_format == 'm/d/Y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('m/d/Y') ?>
                            </option>
                            <option value="Y/m/d"
                                    <?php if ($date_format == 'Y/m/d') { ?>selected="selected"<?php } ?> >
                                <?php echo date('Y/m/d') ?>
                            </option>

                            <option value="d.m.y"
                                    <?php if ($date_format == 'd.m.y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('d.m.y') ?>
                            </option>
                            <option value="m.d.y"
                                    <?php if ($date_format == 'm.d.y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('m.d.y') ?>
                            </option>
                            <option value="y.m.d"
                                    <?php if ($date_format == 'y.m.d') { ?>selected="selected"<?php } ?> >
                                <?php echo date('y.m.d') ?>
                            </option>
                            <option value="d.m.Y"
                                    <?php if ($date_format == 'd.m.Y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('d.m.Y') ?>
                            </option>
                            <option value="m.d.Y"
                                    <?php if ($date_format == 'm.d.Y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('m.d.Y') ?>
                            </option>
                            <option value="Y.m.d"
                                    <?php if ($date_format == 'Y.m.d') { ?>selected="selected"<?php } ?> >
                                <?php echo date('Y.m.d') ?>
                            </option>

                            <option value="d-m-y"
                                    <?php if ($date_format == 'd-m-y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('d-m-y') ?>
                            </option>
                            <option value="m-d-y"
                                    <?php if ($date_format == 'm-d-y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('m-d-y') ?>
                            </option>
                            <option value="y-m-d"
                                    <?php if ($date_format == 'y-m-d') { ?>selected="selected"<?php } ?> >
                                <?php echo date('y-m-d') ?>
                            </option>
                            <option value="d-m-Y"
                                    <?php if ($date_format == 'd-m-Y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('d-m-Y') ?>
                            </option>
                            <option value="m-d-Y"
                                    <?php if ($date_format == 'm-d-Y') { ?>selected="selected"<?php } ?> >
                                <?php echo date('m-d-Y') ?>
                            </option>
                            <option value="Y-m-d"
                                    <?php if ($date_format == 'Y-m-d') { ?>selected="selected"<?php } ?> >
                                <?php echo date('Y-m-d') ?>
                            </option>
                        </select>
                    </div>
                    <div class="col-sm-8 wpat_description">
                        <?php _e('Select the date formatting', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label class="col-sm-2" for="wpat_number_format">
                        <?php _e('Number formatting', 'activetables'); ?>
                    </label>
                    <div class="col-sm-2">
                        <select name="wpat_number_format" id="wpat_number_format" class="form-control"
                                ng-init="form.number_format='<?php echo ($number_format) ? $number_format : 'space' ?>'"
                                ng-model="form.number_format">
                            <option value="dot"
                                    <?php if ($number_format == 'dot') { ?>selected="selected"<?php } ?>>
                                12.345,67
                            </option>
                            <option value="comma"
                                    <?php if ($number_format == 'comma') { ?>selected="selected"<?php } ?>>
                                12,345.67
                            </option>
                            <option value="space"
                                    <?php if ($number_format == 'space') { ?>selected="selected"<?php } ?>>
                                12 345,67
                            </option>
                        </select>
                    </div>
                    <div class="wpat_description col-sm-8">
                        <?php _e('Select the number formatting (thousands and also decimals separator)', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label class="col-sm-2" for="wpat_fraction_size">
                        <?php _e('Fraction size', 'activetables'); ?>
                    </label>
                    <div class="col-sm-2">
                        <select name="wpat_fraction_size" id="wpat_fraction_size" class="form-control"
                                ng-init="form.fraction_size='<?php echo ($fraction_size) ? $fraction_size : 2 ?>'"
                                ng-model="form.fraction_size">
                            <option value="0"
                                    <?php if ($fraction_size == '0') { ?>selected="selected"<?php } ?>>
                                12
                            </option>
                            <option value="1"
                                    <?php if ($fraction_size == '1') { ?>selected="selected"<?php } ?>>
                                12.3
                            </option>
                            <option value="2"
                                    <?php if ($fraction_size == '2') { ?>selected="selected"<?php } ?>>
                                12.34
                            </option>
                        </select>
                    </div>
                    <div class="col-sm-8 wpat_description">
                        <?php _e('Number of decimal places to round the number to', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_purchase_code" class="col-sm-2">
                        <?php _e('Envato purchase code', 'activetables'); ?>
                    </label>
                    <div class="col-sm-2">
                        <input type="text" name="wpat_purchase_code" id="wpat_purchase_code" class="form-control"
                               ng-init="form.purchase_code='<?php if ($purchase_code) echo $purchase_code ?>'"
                               ng-model="form.purchase_code"/>
                    </div>
                    <div class="col-sm-8 wpat_description">
                        <?php _e('To get plugin auto-updates please enter Envato purchase code. If you do not want plugin to be able to auto-update, keep it clear', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <div class="col-sm-2">
                        <input type="submit" name="submit" class="btn btn-primary"
                               value="<?php _e('Save', 'activetables'); ?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <?php _e('Color and font settings', 'activetables'); ?>
                </h3>
            </div>
            <div class="box-body">
                <p><?php _e('Leave the setting blank to use default value', 'activetables'); ?></p>
                <div class="form-group clearfix">
                    <label for="wdtTableFontColor" class="col-sm-2">
                        <?php _e('Table font color', 'activetables'); ?>
                    </label>

                    <div class="col-sm-2">
                        <input type="text" name="wdtTableFontColor" id="wdtTableFontColor" class="form-control"
                               ng-init="form.font_color='<?php echo(!empty($wdtFontColorSettings['wdtTableFontColor']) ? $wdtFontColorSettings['wdtTableFontColor'] : '') ?>'"
                               ng-model="form.font_color"/>
                    </div>
                    <div class="wpat_description col-sm-8">
                        <?php _e('This color is used for the main font in table cells', 'activetables'); ?>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <div class="col-sm-12">
                        <input type="submit" name="submit" class="btn btn-primary"
                               value="<?php _e('Save', 'activetables'); ?>">
                        <button class="btn btn-default">
                            <?php _e('Reset colors and fonts to default', 'activetables'); ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <?php _e('Custom JS and CSS', 'activetables'); ?>
                </h3>
            </div>
            <div class="box-body">
                <p><?php _e('Leave the setting blank to use default value', 'activetables'); ?>.</p>
                <div class="form-group clearfix">
                    <label for="wpat_custom_js" class="col-sm-2">
                        <?php _e('Custom ActiveTables JS', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <textarea name="wpat_custom_js" id="wpat_custom_js" class="form-control"
                                  ng-model="form.custom_js"
                                  ng-init="form.custom_js='<?php echo(!empty($custom_js) ? stripslashes($custom_js) : '') ?>'"></textarea>
                        <div class="wpat_description">
                            <?php _e('This JS will be inserted as an inline script block on every page that has a wpDataTable', 'activetables'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_custom_css" class="col-sm-2">
                        <?php _e('Custom ActiveTables CSS', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <textarea name="wpat_custom_css" id="wpat_custom_css" class="form-control"
                                  ng-model="form.custom_css"
                                  ng-init="form.custom_css='<?php echo (!empty($custom_css)) ? $custom_css : '' ?>'">
                        </textarea>
                        <div class="wpat_description">
                            <?php _e('This CSS will be inserted as an inline style block on every page that has a wpDataTable', 'activetables'); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label for="wpat_minified" class="col-sm-2">
                        <?php _e('Use minified wpDataTables Javascript', 'activetables'); ?>
                    </label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="wpat_minified" name="wpat_minified"
                               <?php if (!empty($minified)) { ?>ng-init="form.minified=true"<?php } ?>
                               ng-false-value="0"
                               ng-model="form.minified"/>
                        <span class="wpat_description">
                            <?php _e('Uncheck if you would like to make some changes to the main wpDataTables JS file (assets/js/activetables/activetables.js). Minified is inserted by default (better performance)', 'activetables'); ?>
                        </span>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <div class="col-sm-2">
                        <input type="submit" name="submit" class="btn btn-primary"
                               value="<?php _e('Save', 'activetables'); ?>">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    //    jQuery(document).ready(function () {
    //        // exColorPicker
    //        jQuery('#wdtHeaderBaseColor').wpColorPicker();
    //
    //        jQuery('#wpDataTablesSettings').submit(function (e) {
    //            e.preventDefault();
    //            e.stopImmediatePropagation();
    //            var data = {
    //                action: 'wdt_save_settings',
    //                wpUseSeparateCon: (jQuery('#wpUseSeparateCon').attr('checked') == 'checked'),
    //                wpMySqlHost: jQuery('#wpMySqlHost').val(),
    //                wpMySqlDB: jQuery('#wpMySqlDB').val(),
    //                wpMySqlUser: jQuery('#wpMySqlUser').val(),
    //                wpMySqlPwd: jQuery('#wpMySqlPwd').val(),
    //                wpMySqlPort: jQuery('#wpMySqlPort').val(),
    //                wpRenderFilter: jQuery('#wpRenderFilter').val(),
    //                wpInterfaceLanguage: jQuery('#wpInterfaceLanguage').val(),
    //                wpDateFormat: jQuery('#wpDateFormat').val(),
    //                wpTopOffset: '',
    //                wpLeftOffset: '',
    //                wdtTablesPerPage: jQuery('#wdtTablesPerPage').val(),
    //
    //            };
    //            jQuery('#wdtPreloadLayer').show();
    //            jQuery.post(ajaxurl, data, function (response) {
    //                jQuery('#wdtPreloadLayer').hide();
    //                if (response == 'success') {
    //                    wdtAlertDialog('<?php //_e('Settings saved successfully', 'activetables'); ?>//', '<?php //_e('Success!', 'activetables'); ?>//');
    //                } else {
    //                    wdtAlertDialog('<?php //_e('There was a problem saving your settings', 'activetables'); ?>//', '<?php //_e('Error!', 'activetables'); ?>//');
    //                }
    //            });
    //        });
    //    });
</script>