<?php

class Activetables_Admin_Ajax
{
    public function save_table()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        global $wpdb;

        $_POST = apply_filters('activetables_filter_before_save_table', $_POST);
        $table_id = filter_var($_POST['table_id'], FILTER_SANITIZE_NUMBER_INT);
        $table_title = sanitize_text_field($_POST['table_title']);
        $show_title = (int)$_POST['show_title'];
        $table_source = sanitize_text_field($_POST['table_source']);
        $file_url = filter_var($_POST['file_url'], FILTER_SANITIZE_URL);
        $table_lang = sanitize_text_field($_POST['table_lang']);
        $table_width = sanitize_text_field($_POST['table_width']);
        $table_height = sanitize_text_field($_POST['table_height']);
        $row_height = filter_var($_POST['row_height'], FILTER_SANITIZE_NUMBER_INT);
        $column_size = filter_var($_POST['column_size'], FILTER_SANITIZE_NUMBER_INT);
        $column_menus = (int)$_POST['column_menus'];
        $on_load = (int)$_POST['on_load'];
        $menu_show_hide = (int)$_POST['menu_show_hide'];
        $filtering = (int)$_POST['filtering'];
        $sorting = (int)$_POST['sorting'];
        $pinning = (int)$_POST['pinning'];
        $column_resizing = (int)$_POST['column_resizing'];
        $export = (int)$_POST['export'];
        $word_wrap = (int)$_POST['word_wrap'];
        $grid_footer = (int)$_POST['grid_footer'];
        $column_footer = (int)$_POST['column_footer'];

        if (!$table_id) {
            $data = array(
                'title' => $table_title,
                'show_title' => $show_title,
                'source' => $table_source,
                'source_url' => $file_url,
                'filtering' => $filtering,
                'sorting' => $sorting,
                'column_menus' => $column_menus,
                'grid_menu_show_hide_columns' => $menu_show_hide,
                'grid_footer' => $grid_footer,
                'column_footer' => $column_footer,
                'row_height' => $row_height,
                'min_column_size' => $column_size,
                'table_width' => $table_width,
                'table_height' => $table_height,
                'export' => $export,
                'pinning' => $pinning,
                'column_resizing' => $column_resizing,
                'table_lang' => $table_lang,
                'word_wrap' => $word_wrap,
                'on_load' => $on_load
            );
            $wpdb->insert(WPAT_TBL_SETS, $data);
            $table_id = $wpdb->insert_id;
            echo $table_id;
        } else {
            $data = array(
                'title' => $table_title,
                'show_title' => $show_title,
                'source' => $table_source,
                'source_url' => $file_url,
                'filtering' => $filtering,
                'sorting' => $sorting,
                'column_menus' => $column_menus,
                'grid_menu_show_hide_columns' => $menu_show_hide,
                'grid_footer' => $grid_footer,
                'column_footer' => $column_footer,
                'row_height' => $row_height,
                'min_column_size' => $column_size,
                'table_width' => $table_width,
                'table_height' => $table_height,
                'export' => $export,
                'pinning' => $pinning,
                'column_resizing' => $column_resizing,
                'table_lang' => $table_lang,
                'word_wrap' => $word_wrap,
                'on_load' => $on_load
            );
            $wpdb->update(WPAT_TBL_SETS, $data, array('id' => $table_id));
        }
    }

    public function save_settings()
    {
        global $wpdb;
        $_POST = json_decode(file_get_contents('php://input'), true);
        $_POST = apply_filters('activetables_filter_before_save_settings', $_POST);
//        var_dump($_POST);
        $default_language = sanitize_text_field($_POST['default_lang']);
        $date_format = sanitize_text_field($_POST['date_format']);
        $fraction_size = (int)$_POST['fraction_size'];
        $number_format = sanitize_text_field($_POST['number_format']);
        $purchase_code = sanitize_text_field($_POST['purchase_code']);
        $custom_css = sanitize_text_field($_POST['custom_css']);
        $custom_js = $_POST['custom_js'];
        $minified = (int)$_POST['minified'];

        update_option('wpat_default_language', $default_language);
        update_option('wpat_date_format', $date_format);
        update_option('wpat_number_format', $number_format);
        update_option('wpat_fraction_size', $fraction_size);
        update_option('wpat_purchase_code', $purchase_code);
        update_option('wpat_custom_css', $custom_css);
        update_option('wpat_custom_js', $custom_js);
        update_option('wpat_minified', $minified);

        $response['message'] = 'Success';
        echo json_encode($response);
        die();
    }
}
